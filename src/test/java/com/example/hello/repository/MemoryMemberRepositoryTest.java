package com.example.hello.repository;

import com.example.hello.domain.Member;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;    // import 후 static import시 코드 줄일 수 있음.

public class MemoryMemberRepositoryTest {

    /*
     * 테스트 주도 개발 (TDD) :
     *    자신이 개발할 프로젝트에 대해,
     *    먼저 틀을 만들어 테스트를 시행하고 메소드를 구현 테스트.
     *    강의 내용은 TDD가 아니다. (TDD는 구현 이전에 테스트)
     *    협업시, 코드가 몇만 몇십만 라인이 넘어가면 테스트 코드 없이는 작업이 불가능.
     *
     */


    MemoryMemberRepository repository = new MemoryMemberRepository();

    @AfterEach // 테스트 완료 후 호출될 콜백 메소드
    public void afterEach(){
        repository.clearStore();    // Test 메소드마다 clear한다.
                                    // 테스트는 순서 의존관계가 없어야한다.
                                    // if not, 순서가 보장되지 않아 오류 발생
    }

    @Test
    public void save(){

        Member member = new Member();
        member.setName("spring");

        repository.save(member);

        Member result = repository.findById(member.getId()).get(); // 반환타입 optional에서 값을 꺼낼때는 .get()으로.
                                                                   // Optional로 감싸지 않아도 됨.
                                                                   // 좋은 방식은 아니지만 test에서는 괜찮.

        //System.out.println("result = " + (result == member));

        //Assertions.assertEquals(member, null);   // null의 경우 test 실패
        //Assertions.assertEquals(member, result); // expected값이 앞에, actual이 뒤에
        assertThat(member).isEqualTo(result);      // 실무에서는 build 툴과 엮어서 test 미통과시 다음 단계를 막는 방식으로 활용한다.

    }

    @Test
    public void findByName(){

       Member member1 = new Member();
       member1.setName("spring1");
       repository.save(member1);

       Member member2 = new Member();
       member2.setName("spring2");
       repository.save(member2);

       Member result = repository.findByName("spring1").get();

       assertThat(result).isEqualTo(member1);

    }

    @Test
    public void finaAll(){

       Member member1 = new Member();
       member1.setName("spring1");
       repository.save(member1);

       Member member2 = new Member();
       member2.setName("spring2");
       repository.save(member2);

        List<Member> result = repository.findAll();

        assertThat(result.size()).isEqualTo(2);
    }


}
