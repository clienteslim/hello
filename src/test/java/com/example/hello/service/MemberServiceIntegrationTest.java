package com.example.hello.service;

import com.example.hello.domain.Member;
import com.example.hello.repository.MemberRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


// Test 클래스 생성 : 서비스 문장 클릭 후 ctrl shift T
// 실행 단축키 shift F10

@SpringBootTest
@Transactional // commit 하기전까지는 반영X.
               // 테스트케이스에서 @Transactional을 사용하면 테스트 후 db를 rollback함. (지우는게 아니고 아예 반영을 안함)
               // @Commit의 경우는 commit함.
               // 다른 곳에 Transactional을 사용했을때는 다름.
               // afterEach 사용 안해도 됨.
class MemberServiceIntegrationTest {

    //테스트에서는 다른 객체에서 참조하지 않기 때문에 autowired로 필드주입 처리해도 무방
    @Autowired MemberService memberService;

    //MemoryMemberRepository memberRepository;

    @Autowired
    MemberRepository memberRepository;



/*
    @BeforeEach
    public void beforeEach(){
        memberRepository = new MemoryMemberRepository();
        memberService = new MemberService(memberRepository); // memberService와 동일한 객체 사용 (=> DI 의존성 주입)

    }*/

/*
    @AfterEach // 테스트 완료 후 호출될 콜백 메소드
    public void afterEach(){
        memberRepository.clearStore();    // Test 메소드마다 clear한다.
        // 테스트는 순서 의존관계가 없어야한다.
        // if not, 순서가 보장되지 않아 오류 발생
    }*/

    @Test
    void join() {
        // given 주어져서
        Member member = new Member();
        member.setName("spring");

        // when  뭘 했을 때
        Long saveId = memberService.join(member);

        // then  이게 나와야해
        Member findMember = memberService.findOne(saveId).get();
        assertThat(member.getName()).isEqualTo(findMember.getName());
    }

    @Test
    public void 중복_회원_예외(){

        //given
        Member member1 = new Member();
        member1.setName("spring");

        Member member2 = new Member();
        member2.setName("spring");

        //when
        memberService.join(member1);

        //memberService로 member2를 join하면 에러가 터져야해.
        IllegalStateException e = assertThrows(IllegalStateException.class, () -> memberService.join(member2));

        assertThat(e.getMessage()).isEqualTo("이미 존재하는 회원입니다.");

    }


    @Test
    void findMembers() {
    }

    @Test
    void findOne() {
    }
}