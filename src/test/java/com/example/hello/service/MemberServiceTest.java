package com.example.hello.service;

import com.example.hello.domain.Member;
import com.example.hello.repository.MemoryMemberRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;


// Test 클래스 생성 : 서비스 문장 클릭 후 ctrl shift T
// 실행 단축키 shift F10

class MemberServiceTest {

    MemberService memberService;

    MemoryMemberRepository memberRepository;

    @BeforeEach
    public void beforeEach(){
        memberRepository = new MemoryMemberRepository();
        memberService = new MemberService(memberRepository); // memberService와 동일한 객체 사용 (=> DI 의존성 주입)

    }

    @AfterEach // 테스트 완료 후 호출될 콜백 메소드
    public void afterEach(){
        memberRepository.clearStore();    // Test 메소드마다 clear한다.
        // 테스트는 순서 의존관계가 없어야한다.
        // if not, 순서가 보장되지 않아 오류 발생
    }

    @Test
    void join() {
        // given 주어져서
        Member member = new Member();
        member.setName("spring");

        // when  뭘 했을 때
        Long saveId = memberService.join(member);

        // then  이게 나와야해
        Member findMember = memberService.findOne(saveId).get();
        assertThat(member.getName()).isEqualTo(findMember.getName());
    }

    @Test
    public void 중복_회원_예외(){

        //given
        Member member1 = new Member();
        member1.setName("spring");

        Member member2 = new Member();
        member2.setName("spring");

        //when
        memberService.join(member1);

        //memberService로 member2를 join하면 에러가 터져야해.
        IllegalStateException e = assertThrows(IllegalStateException.class, () -> memberService.join(member2));

        assertThat(e.getMessage()).isEqualTo("이미 존재하는 회원입니다.");

        // 편의상 try catch 대신할 문법을 제공함.
        /*try {

            memberService.join(member2);
            fail();
        } catch (IllegalStateException e){
            assertThat(e.getMessage()).isEqualTo("이미 존재하는 회원입니다.");

        }*/



        //then

    }


    @Test
    void findMembers() {
    }

    @Test
    void findOne() {
    }
}