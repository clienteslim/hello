package com.example.hello.repository;

import com.example.hello.domain.Member;

import java.util.*;

//@Repository
public class MemoryMemberRepository implements MemberRepository{

    private static Map<Long, Member> store = new HashMap<>();   // 실무에서는 동시성 문제때문에 concurrentHashMap 사용
    private static long sequence = 0L;                          // 실무에서는 동시성 문제 : AtomicLong을 사용.


    @Override
    public Member save(Member member) {

        member.setId(++sequence);           // sequence가 증가하지 않으면, member에 2개 이상 값 테스트시 에러 발생
                                            // (db로 예를 들면 제약 조건을 위배하기 때문일듯 )
        store.put(member.getId(), member);

        return member;
    }

    @Override
    public Optional<Member> findById(Long id) {

        return Optional.ofNullable(store.get(id));
    }

    @Override
    public Optional<Member> findByName(String name) {
        return store.values().stream()
                      .filter(member->member.getName().equals(name))
                      .findAny();     // 람다활용 -> 루프를 돌린다. 필터링을 통해 해당값을 하나라도 찾으면 반환.

    }

    @Override
    public List<Member> findAll() {

        return new ArrayList<>(store.values());
    }

    public void clearStore(){

        store.clear();

    }

}
