package com.example.hello.repository;

import com.example.hello.domain.Member;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class JDBCTemplateMemberRepository implements MemberRepository{
    //JDBCTemplate은 실무에서도 많이 활용함.

    private final JdbcTemplate jdbcTemplate;

    public JDBCTemplateMemberRepository(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Member save(Member member) {

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

        jdbcInsert.withTableName("member").usingGeneratedKeyColumns("id");      //별도의 쿼리문 없이 Table의 PK를 토대로 Insert문 쿼리 형성

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("name", member.getName());

        Number key = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));
        member.setId(key.longValue());

        return member;
    }


    @Override
    public Optional<Member> findById(Long id) {
        List<Member> result = jdbcTemplate.query("select * from member where id = ?", memberRowMapper(), id);
        return result.stream().findAny();
    }

    @Override
    public Optional<Member> findByName(String name) {
        List<Member> result = jdbcTemplate.query("select * from member where name = ?", memberRowMapper(), name);
        return result.stream().findAny();
    }

    @Override
    public List<Member> findAll() {
        return jdbcTemplate.query("select * from member", memberRowMapper());
    }

    // 객체 생성은 여기서 콜백으로 이뤄짐
   private RowMapper<Member> memberRowMapper(){

        //return new RowMapper<Member>() { // => 람다로 바꿀 수 있음.
        return (rs, rowNum) -> {

            Member member = new Member();

            member.setId(rs.getLong("id"));
            member.setName(rs.getString("name"));

            return member;
        };
    }

}
