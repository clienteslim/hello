package com.example.hello.repository;


import com.example.hello.domain.Member;

import java.util.List;
import java.util.Optional;


public interface MemberRepository {

    Member save(Member member);
    Optional<Member> findById(Long id); //Optional => Java 8에 나온 기능. Null을 Optional로 감싸서 반환한다.
    Optional<Member> findByName(String name);
    List<Member> findAll();

}
