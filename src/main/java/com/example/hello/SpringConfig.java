package com.example.hello;

import com.example.hello.aop.TimeTraceAop;
import com.example.hello.repository.MemberRepository;
import com.example.hello.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {


    private final MemberRepository memberRepository;

    @Autowired
    public SpringConfig(MemberRepository memberRepository) {

        this.memberRepository = memberRepository;
    }
/*    @PersistenceContext
    private EntityManager em;

    @Autowired
    public SpringConfig(EntityManager em) {
        this.em = em;
    }*/


/*    private DataSource dataSource;

    public SpringConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }*/

    @Bean
    public MemberService memberService(){

        //return new MemberService(memberRepository());
        return new MemberService(memberRepository);
    }
    
    // 이처럼 Bean으로 직접 등록하지 않고 TimeTraceAop클래스상에 @Component을 사용해도 된다.
    @Bean
    public TimeTraceAop timeTraceAop(){

        return new TimeTraceAop();
    }

//    @Bean
//    MemberRepository memberRepository(){

        //return new MemoryMemberRepository();
        //return new JdbcMemberRepository(dataSource);
        //return new JDBCTemplateMemberRepository(dataSource);
        //return new JpaMemberRepository(em);
//    }





}
