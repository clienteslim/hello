package com.example.hello.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
//@Component
public class TimeTraceAop {

    @Around("execution(* com.example.hello..*(..)) && !target(com.example.hello.SpringConfig)")
                // SpringConfig에 bean을 등록해서 사용하는 경우. 순환 참조를 막기 위해 SpringConfig는 제외함.
    //@Around("execution(* com.example.hello..*(..))")
                // 순환참조가 발생하지 않음. @Component와 같이 사용. 타게팅을 정해준다. 패키지 하위에 전부 적용하라.
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        System.out.println("START : " + joinPoint.toLongString());  //어떤 메소드를 call 하는지 알 수 있음.

        try{
            //Object result = joinPoint.proceed();            // 한줄로 inline 만들기 (return result; 적고 ) + ctrl alt n
            return joinPoint.proceed();


        } finally{

           long finish = System.currentTimeMillis();
           long timeMs = finish - start;
            System.out.println("END : " + joinPoint.toLongString() + " " + timeMs + "ms" );
        }



    }

}
