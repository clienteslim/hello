package com.example.hello.service;

import com.example.hello.domain.Member;
import com.example.hello.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

//@Service
@Transactional // jpa에서 join은 Transactional 안에서 실행돼야함.
public class MemberService {

    // Test에서도 이러한 인스턴스 생성시 매번 다른 인스턴스를 생성할 수 있으므로 이 구문은 아래와 같이 바꾼다.
    // Test에는 @BeforeEach 구문 적용

    //private final MemberRepository memberRepository = new MemoryMemberRepository();

    private final MemberRepository memberRepository;

    @Autowired
    public MemberService(MemberRepository memberRepository) {

        this.memberRepository = memberRepository;
    }

    /**
     * 회원가입
     */
    public Long join(Member member){

        long start = System.currentTimeMillis();

        validateDuplicateMember(member);  // 같은 이름이 있는 중복회원 x
        memberRepository.save(member);
        return member.getId();

    }

    /* // ctrl alt v 단축키 : 반환형
     *    Optional<Member> result = memberRepository.findByName(member.getName());
     *
     *
     *       // result.orElseGet() : 값이 있으면 꺼내고 없으면 디폴트값을 꺼내라 등.
     *       // ifPresent : Optional로 감쌌기 때문에 활용가능한 메소드
     *
     *        result.ifPresent(m->{
     *                   throw new IllegalStateException("이미 존재하는 회원입니다.")
     *        });
     */



    /* 리팩토링 관련 단축키 : ctrl alt M (extract Method)
    *                       (refactor this 전체 메뉴 : shift ctrl alt T)
    * => Optional을 바로 반환하는 걸 권장하지 않음.
    */
    private void validateDuplicateMember(Member member) {

        memberRepository.findByName(member.getName())
                .ifPresent(m->{
                    throw new IllegalStateException("이미 존재하는 회원입니다.");

                });
    }

    public List<Member> findMembers(){

        return memberRepository.findAll();
    }

    public Optional<Member> findOne(Long memberId){

        return memberRepository.findById(memberId);
    }

}
