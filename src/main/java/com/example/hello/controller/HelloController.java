package com.example.hello.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @GetMapping("hello")
    public String hello(Model model){

        model.addAttribute("data", "spring!!");

        return "hello";

    }

    @GetMapping("hello-mvc")
    public String helloMvc(@RequestParam(value="name", required = false) String name, Model model) {

        model.addAttribute("name", name);

        return "hello-template";
    }

    @GetMapping("hello-string")
    @ResponseBody // 응답 body부에 직접 입력한다. 이 경우는 name이 입력됨.
    public String helloString(@RequestParam("name") String name){

        return "hello" + name;

    }

    /* API 방식 : 문자가 아닌 객체를 내려준다. */
    @GetMapping("hello-api")
    @ResponseBody
    public Hello helloApi(@RequestParam("name") String name){

        Hello hello = new Hello(); // Tip : ctrl shift enter (괄호 자동완성)
        hello.setName(name);
        return hello;
    }

    static class Hello{
        private String name;        /* 자바빈 표준방식 : private로 제한하고
                                     * public getter setter로 값을 가져올 수 있게
                                     * property 접근방식
                                     */

        //private String getName(){
        // 실수 체크 : private로 접근제한자를 걸으면
        // 객체에서 getter를 하지 못한다.
        public String getName(){

            return name;
        }
        public void setName(String name){

            this.name = name;
        }

    }

}
