package com.example.hello.controller;

import com.example.hello.domain.Member;
import com.example.hello.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller // 컨트롤러 어노테이션 : 스프링 컨테이너에 컨트롤러 객체(빈)가 생성됨.
public class MemberController {

    //private final MemberService memberService = new MemberService();
    // 굳이 객체를 여러번 생성할 이유가 없음.

    private final MemberService memberService;

    @Autowired // DI (의존성 주입)
    public MemberController(MemberService memberService) {

        this.memberService = memberService;

    }

    @GetMapping("/members/new")
    public String createForm(){

        return "members/createMemberForm";
    }

    @PostMapping("/members/new")
    public String create(MemberForm form){

        Member member = new Member();
        member.setName(form.getName());

        memberService.join(member);

        return "redirect:/";
    }

    @GetMapping("members")
    public String list(Model model){
        List<Member> members = memberService.findMembers();
        model.addAttribute("members", members);

        return "members/memberList";
    }
}
